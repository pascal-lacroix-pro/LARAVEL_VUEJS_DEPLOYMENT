// ./stores/UsersStore.js
import { ref } from 'vue';

export const user_test = ref([]);

export async function fetchUserTest() {
  try {
    const response = await fetch('http://127.0.0.1:8000/api/user_test');
    const data = await response.json();
    user_test.value = data;
  } catch (error) {
    console.error('Erreur lors du fetch des utilisateurs:', error);
  }
}
